# Avrio
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

A c/c++ implementation of a multi blockchain cryptocurrency featuring multiple chains, proof of node and no PoW! Join our discord for more info http://discord.gg/R3UhgCk

## TODO:
- [x] refactor the code

- [ ] **Blockchain**:
-   [ ] Store blockchain on drive rather than current implementation where it is stored on ram! (WIP)
- [ ] **P2p**:
- [x] make connections to peers (WIP)
- [ ] share blocks with peers
- [ ] share transactions with peers
- [ ] get blocks from peers
- [ ] submit uptime proof
- [ ] **Wallet**:
- [ ] *(Wallet) Core*:
- [x] ~~Create rawtx~~
- [ ] sign rawtx
- [ ] ask node for new transactions
- [ ] generate public & private keys
- [ ] Make a nice cli interface
- [ ] Create, save and open wallet files
- [ ] **RPC**:
- [ ] Daemon (getBlockHeight, getPeerCount, ect) http rpc api
- [ ] Wallet (sendTranscation, getBalanceForAddress, ect) rpc api
- [ ] Daemon (addBlock, addPeer, ect) json rpc api
- [ ] **Core**:
- [ ] verify uptime proof
- [ ] sign blocks
- [ ] verify submitted blocks
- [ ] verfy transactions
- [ ] **Daemon**:
- [ ] *Daemon Core*
- [ ] Sync blockchain from other peers
- [ ] Make a nice Cli interface
- [ ] **Database**:
- [x] Save Blockchain

