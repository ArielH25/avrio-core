#include "storage.h"
#include <core/blockchain.h>
#include <core/block.h>
#include <src/utils/initilised.cpp> // TODO
#include "src/core/account.cpp"
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <stdio.h>
#include <stdint.h>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <filesystem>

#include "serialisation/serialisation.cpp"
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <assert>
#include "external/rocksdb/db.h"
#include "external/spdlog/spdlog.h"

namespace fs = boost::filesystem;

char getFolder()
{
	char folder;
	snprintf(folder, 256, "~/.%s/", CURRENCY_NAME);
	return (folder);
}

std::optional<Account> getAccount(std::string pk)
{
	Account *acc = new Account;

	// Index
	acc.publicKey = pk;
	fs::path P getFolder() + "/";

	// Load data
	rocksdb::DB *db;
	rocksdb::Options options;
	options.create_if_missing = false;
	rocksdb::Status status = rocksdb::DB::Open(options, p + "db/accountDb", &db);

	if (!status.ok())
		return;

	std::string buffer;
	rocksdb::Status s = db->Get(rocksdb::ReadOptions(), pk, &buffer);

	if (s.ok())
		acc = deserialiseAccount(buffer);

	if (!s.ok())
		return;

	delete db;
	return acc;
}

int setAccount(std::string pk, Account acc)
{
	fs::path P getFolder() + "/";
	std::string buffer = serialiseAccount(acc);

	// save data
	rocksdb::DB *db;
	rocksdb::Options options;
	options.create_if_missing = false;
	rocksdb::Status status = rocksdb::DB::Open(options, p + "db/accountDb", &db);
	assert(status.ok());
	rocksdb::Status s = db->Put(rocksdb::WriteOptions(), pk, buffer);

	if (!s.ok())
		return 0;

	delete db;

	// todo
	return 1;
}

bool createFolder(std::string path)
{
	fs::path P path + "/";
	fs::create_directories(p / ".Avrio");
	fs::path P path + "/.Avrio");
	fs::create_directories(p / "db");
	std::ofstream accDb{p + "db/accounts.aiodb"};
	fs::create_directories(p / "Chains");
	return true;
}

bool createChain(std::string path, std::string key)
{
	fs::path P path + "/";
	fs::create_directories(p / "Chains" / key);
	return true;
}
int saveBlock(std::string chainKey, Block blkS) {
	rocksdb::DB *db;
	rocksdb::Options options;
	options.create_if_missing = true;
	rocksdb::Status status = rocksdb::DB::Open(options, p + "Chains/" + chainKey, &db);
	std::string blkS = serialiseBlock(blk);
	rocksdb::Status s = db->Put(rocksdb::WriteOptions(), chainKey, blkS);
}
bool pushBlock(Block block, std::string chainKey)
{
	saveBlock(chainKey, block);
}

bool popBlock(uint64_t amount, std::string chainKey)
{
	uint64_t height = getHeight(chainKey);

	rocksdb::DB *db;
	rocksdb::Options options;
	options.create_if_missing = false;
	rocksdb::Status status = rocksdb::DB::Open(options, p + "Chains/" + chainKey, &db);

	for (uint64_t i = 0; i < amount; i++)
	{
		s = db->Delete(rocksdb::WriteOptions(), height - i);
	}

	delete db;
	return 1;
}

bool rewindTo(uint64_t height, std::string chainKey)
{
	uint64_t currentHeight = getHeight(chainKey);
	popBlock(currentHeight - height, chainKey);
}

Block getBlockByheight(uint64_t height, std::string chainKey)
{
	Block *block = new Block;

	// Load data
	rocksdb::DB *db;
	rocksdb::Options options;
	options.create_if_missing = false;
	rocksdb::Status status = rocksdb::DB::Open(options, p + "Chains/" + chainkey, &db);

	if (!status.ok())
		return;

	std::string buffer;
	rocksdb::Status s = db->Get(rocksdb::ReadOptions(), height, &buffer);

	if (s.ok())
		block = deserialiseBlock(buffer);

	return block;
}

uint64_t getBlockCount(std::string chainKey)
{
	rocksdb::DB *db;
	rocksdb::Options options;
	options.create_if_missing = false;
	rocksdb::Status status = rocksdb::DB::Open(options, p + "Chains/" + chainKey, &db);
	int num;
	db->GetProperty("rocksdb.estimate-num-keys", &num);

	return num;
}

bool reset(std::string chainKey = "*")
{
	if (chainKey == "*")
	{ // delete all chains
		fs::path P getFolder() + "/Chains";
		if (!std::filesystem::remove_all(p, ec))
		{
			spdlog::error("error deleting chain folder:" + ec);
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		std::error_code ec;
		fs::path P getFolder() + "/Chains" + chainKey;

		if (!std::filesystem::remove_all(p, ec))
		{
			spdlog::error("error deleting chain " + chainKey + ":" + ec);
			return 0;
		}
		else
		{
			return 1;
		}
	}
}
