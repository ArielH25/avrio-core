#include "src/core/block.h"
#include "src/core/account.cpp"
#include <string>
#include "external/rocksdb/db.h"

class Storage
{
public:
	char *getFolder();
	std::optional<Account> getAccount(uint64_t index);
	int setAccount(uint64_t index, Account acc);
	bool createFolder();
	bool createChain(std::string path, std::string hash);
	bool init(const std::string &blocksFilame, const std::string &indexesFilename);
	bool pushBlock(Block block, std::string chainIndex);
	bool popBlock(amount, std::string chainIndex);
	bool rewindTo(uint64_t height, std::string chainIndex);
	Block getBlockByheight(uint64_t height, std::string chainIndex);
	uint64_t getBlockCount(std::string chainIndex);
	bool reset(std::string chainIndex = "*");
}
