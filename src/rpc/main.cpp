#include<string>
#include<iostream>

// static functionName;

struct apiCall
{
	std::string endpoint;
	std::string description;
	std::string function;
};

static const apiCall apiCalls[]
{
	{           "getState",          "Gets the current state of the node",     "getState" },
	{ "getConnectionCount", "Returns number of currently connected peers", "getPeerCount" },
};

int onCall(std::string call)
{ // call = trigered path (eg getState)
	uint64_t noac = sizeof(apiCalls) / sizeof(apiCall);
	uint64_t i = 0;
	while (i < noac && apiCalls[i].endpoint != call)
	{
		i++
	}
	if (i == noac or i == noac - 1)
	{
		unknownCall(call);
		return 0;
	}
	else
	{
		std::string command << apiCalls[i].endpoint + "()";
		//TODO execute command
		return 1;
	}
}
