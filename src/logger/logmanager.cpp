// Copyright the Avrio developers 2019-2020
// This file manages the backtrace and stores a ring buffer of the last 50 log messages and dumps them
// into console if something crashes (eg if a core thread dieswithout being ended (eg running != false)

#include "external/spdlog/spdlog.h"
#include <thread>

namespace log
{

int catchDump()
{
	while (running())
	{ // todo
		if (error)
		{ // todo
			spdlog::dump_backtrace();
			return 0;
		}
	}
}

int initLog(int backtraceCount = 50)
{
	// Log level tests
	spdlog::set_level(spdlog::level::info); // Set global log level to info
	spdlog::debug("This message should not be displayed! If you see this something has gone wrong (or you have manualy changed log level");
	spdlog::enable_backtrace(backtraceCount); // create ring buffer with capacity of bactraceCount messages
	std::thread catchDumpThread catchDump();

	return 1;
}

} // namespace log
