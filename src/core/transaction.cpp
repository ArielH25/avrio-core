struct Transaction
{
    uint64_t amount;
    std::string senderKey;
    std::string receiverKey;
    uint64_t unlockTime;      // The height at which the coins unlock (become spendable) at.
    char extraData = nullptr; // Extra data which can be bound to the transaction (one byte)
    uint32_t gasPrice;
    uint64_t maxGas;
    uint64_t nonce;        // a nonce added to the transaction to prevent transaction replay attacks
    std::string signature; // the Signature of the transaction
};
