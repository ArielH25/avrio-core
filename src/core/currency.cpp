#include <stdint.h>
#include <math.h>
#include <ctime>
#include <stdlib.h>
#include <string>
#include <src/core/transaction.cpp>

namespace walletChain
{ // These commands are for walletchains only, NOT wallet chains

struct Blockheader
{
	uint8_t version; // version of the coin
	std::string chainKey;
	std::string prevHash; // Hash of the last Block
	uint64_t timestamp;   // Time this block was created (UNIX timestamp)
};

Transaction buildTx(uint64_t maxGas, uint64_t amount, uint64_t unlockTime = 0, std::string sender, std::string reciever, std::string privateKey, uint64_t gasPrice)
{
	Transaction tx;
	tx.amount = amount;
	tx.senderKey = sender;
	tx.receiverKey = reciever;

	if (unlockTime == 0)
	{
		tx.unlockTime = time(nullptr) + minimumTxLockTime;
	}
	else
	{
		if (unlockTime > time(nullptr) + minimumTxLockTime)
		{
			tx.unlockTime = unlockTime;
		}
		else
		{
			std::cout << "Bad unlock Time, Aborting!";
			return 0;
		}
	}

	tx.timestamp = std::time(nullptr);
	tx.signature = signTx(tx, sender, privateKey);
	return tx;
}

// Sign basic TX
std::string signTx(Transaction rawTx, std::string publicKey, std::string privateKey)
{
	std::string out;
	sign(out, publicKey, privateKey, rawTx, 26); // Signature = 64 bytes, raw tx = 24 | total = 90 bytes
	return out;
}

Blockheader createBlockHeader(std::string prevHash, uint64_t height, std::string publicKey)
{
	Blockheader blockHeader;
	blockHeader.version = Blockchain::getVersion(time(nullptr));
	blockHeader.chainKey = publicKey;
	blockHeader.prevHash = prevHash;
	blockHeader.timestamp = std::time(nullptr);
	return blockHeader;
}

} // namespace walletChain
