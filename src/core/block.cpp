// Copywrite the Avrio project 2019-2020

#include <iostream>
#include <string>
#include "src/crypto/hash.c"
#include <ctime>
#include "currency.cpp"
#include "block.h"
#include "core.cpp"
#include "src/serialisation/serialise.cpp" // for serializing blocks

std::optional<std::string> hashBlock(Block blk)
{
	if (blk == NULL or blk == nullptr)
		return 0;

	std::string blkS = serialiseBlock(blk);
	crypto::hash(blkS);
}

std::optional<Block> returnHashBlock(Block blk)
{
	std::string hash = hashBlock(blk);
	blk.hash = hash;
	return blk;
}
