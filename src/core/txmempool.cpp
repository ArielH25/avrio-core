#include "src/core/transaction.cpp"
#include "src/core/blockchain.cpp"

struct mempool
{
	uint64_t size;
	std::vector<uint64_t> id;
	std::vector<Transaction> tx; // tx details
};

mempool initMempool()
{
	static mempool memPool;
	memPool.size = 0;
	loadMempool(memPool);
	return memPool;
}

int txInMempool(std::string tx_hash)
{
	for (uint64_t i = 0; i < memPool.size; i++)
	{
		if (memPool.tx.at(i).hash == tx_hash)
			return 1;
	}

	return 0;
}

Transaction getTxFromMemPool(uint64_t id)
{
	if (id < memPool.size)
		return 0;
	else
		return memPool.tx.at(id);
}

int addTxToMemPool(Transaction tx)
{
	memPool.size++;
	memPool.id.pushBack(memPool.size);
	memPool.tx.pushBack(tx);
	return 1;
}
