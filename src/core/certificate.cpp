// Copywrite the avrio project developers

// this file includes functions related to full nodes registration certificates
#include <ctime>
#include "blockchain.cpp"
#include "core.cpp"
#include "currency.cpp"
#include "account.cpp"
#include <src/config.h>
#include <src/crypto/signatures.cpp>
#include <src/crypto/keypair.cpp>
#include <string>

namespace fullnode
{

namespace certificate
{

struct Certificate
{
	std::string subjectPublicKey;
	std::string subjectSignature;
	uint64_t activationTime; // the time at which the certificate was activated at
	uint64_t timestamp;
};

Certificate constructCertificate(pk, s)
{ // pk = public key, s = signature
	rawCertificate cert;
	t = time(nullptr);
	cert.nodePublicKey = pk;
	setState(pk, 1); // set acount state to fullnode
	cert.subjectSignature = s;
	cert.activationTime = t + certificateActivationDelay;
	cert.timestamp = t;
	return cert;
}

std::string createSignature(keypair keys)
{
	return sign(keys.publicKey, keys.privateKey);
}

int addFullNode(Certificate cert)
{
	std::string certS = serilializeCertificatee(cert);

	if (db::saveToDb("certificatedb", cert.subjectPublicKey, cert))
		return 1;
}

} // namespace certificate

} // namespace fullnode
