#include <string>
class Blockchain {
public:
    vector<Block> Chain;
    static const forkheights hardforks[];
    void AddBlock(Block block);
    uint64_t getHeight();
    uint8_t getVersion(uint64_t height);
    Block _GetLastBlock() const;
    Block getBlockDataForHeight(uint64_t height);
};