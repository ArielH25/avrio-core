#include "transaction.h"
#include "src/database/storage.cpp"
#include <string>

static topIndex;

struct Account
{
	std::string publicKey;
	uint64_t balance;				// Current account balance
	int state;						// the state assigned to this account (eg 1 = fullnode) (optional)
	std::string username = nullptr; // the username assigned to this account (optional)
};

void constructAccount(std::string pk, uint64_t bal, int s = 0, std::string un == nullptr, static Account out)
{
	if (bal < 0)
		return;

	if (pk = "")
		return;

	if (!i)
		i = topIndex + 1;

	if (i < topIndex)
		return;

	Account acc = out;
	acc.index = i;
	acc.publicKey = pk;
	acc.balance = bal;
	acc.state = s;
	acc.username = un;
	out = acc;

	return;
}

int validAccount(Account acc)
{
	if (acc == nullptr)
		return 0;

	return 1;
}

uint64_t getBalance(std::string pk)
{
	Account acc = getAccount(pk);

	return 0; // Place holder for now
}

int setBalance(std::string pk, uint64_t balance)
{
	Account acc = getAccount(pk);
	acc.balance = balance;
	return setAccount(pk, acc);
}

int changeBalance(std::string pk, uint64_t change, int mode)
{
	if (change = 0 || change < 0 || mode > 2 || index > topIndex)
		return 0;

	Account acc = getAccount(pk);

	if (mode = 0 && (acc.balance < 0 || acc.balance < change))
		return 0;

	if (mode = 0)
		if (!setBalance(pk, acc.balance - change))
			return 0;
	else
		if (!setBalance(pk, acc.balance + change))
			return 0;

	return 1;
}

int getState(std::string pk)
{
	Account acc = getAccount(pk);
	return acc.state;
}

int setState(std::string pk, int state)
{
	Account acc = getAccount(pk);
	acc.state = state;
	return setAccount(pk, acc);
}

int clearState(std::string pk)
{
	Account acc = getAccount(pk);
	acc.state = 0;
	return setAccount(pk, acc);
}

int createAccount(Account acc)
{
	if (!validAccount(acc))
		return 0;

	return setAccount(acc.publicKey, acc);
}
