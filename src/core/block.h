#include <src/config.h>
#include <src/core/currency.cpp>
#include <string>
#include "transacion.h"

class Block
{
public:
	Blockheader blockHeader;       // The header of the block
	uint32_t index;                // height of this block
	std::string hash;              // The hash of the block
	std::vector<Transaction> txns; // All transcations in the block in a vector
	std::string data = nullptr;    // Extra data that can be appended to blocks (for example text or a smart contract)
	//                                - The larger this field the higher the fee and the max size is defined in config.h
	std::string signature;         // the signature of the wallet creating this block
	std::string nodeSignature;     // the signature of the node that verifyed the block first, this is how nodes can
	//                                work out who verifyed what. this is included in the hash so ifs its changed its
	//                                obvious and can be rejected
};
