#include <iostream.h>
#include <math.h>
#include "block.h"
#include "transaction.h"
#include "txmempool.cpp"
#include <src/config.h>
#include "external/spdlog/spdlog.h"

namespace core
{

int verifyBlock(Block block)
{
	uint64_t hash;
	uint64_t height, timestamp;
	hash = block.hash;
	height = block.height;
	timestamp = block.timestamp;

	if (!validateHash(block))
		return 0;

	if (blockExists(hash))
	{
		spdlog::debug("block " + hash + "already exists!");
		return 0;
	}
	else {
		return 1;
	}
}

int validateTx(transaction tx)
{
	// check if tx in memPool
	Transaction txPool[memPool.size];

	for (uint64_t i = 0; i < memPool.size; i++)
	{
		txPool[i] = memPool.tx.at(i);

		if (txPool[i] == tx)
			return 0;
	}

	if (tx == NULL)
		return 0;

	if (tx.gasPrice < minimumGasPrice or tx.gasPrice < fullnode::nodeMinimumGasPrice)
		return 0;

	uint64_t accountBalance = getAccount(getIndex(tx.senderKey));

	if (tx.amount > accountBalance)
		return 0;

	uint32_t tx_size = sizeof(tx);

	if (tx_size > MAX_TX_SIZE)
	{
		spdlog::debug("Transaction is too big! %u!", tx.hash, tx_header_size);
		return 0;
	}

	// check signatures
	if (validateTxSignature(tx))
		return 0;

	// if coinbase tx, check amounts
	if (tx.senderKey == NULL)
		if (tx.amount != config::blockReward)
			return 0;
}

} // namespace core
