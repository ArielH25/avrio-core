#include <blockchain.h>
#include <src/config.h>
#include "src/database/storage.cpp"
#include "src/core/block.h"
#include "external/spdlog/spdlog.h"
namespace walletChain {
  struct fork_heights
  {
    uint8_t version;
    uint64_t timestamp;
  };

// Hard forks
 static const fork_heights hard_forks[] =
  {
    { 1, 0 },
  };

  if (hard_forks.size() < 1) {
    spdlog::error("ERROR: fork heights array cannot be under 1 (0 or under)");
    exit(-1);
  }

// Add a block
    int Blockchain::addBlock(Block block, std::string chainKey) {
        if (!verify(Block block)) {
            return 0;
        }else{
            pushBlock(block,chainKey); 
            return 1;
       }
    }
   uint64_t Blockchain::getHeight(std::string chainKey) {
       return getBlockCount(chainKey);
   }

  uint8_t Blockchain::getVersion(uint64_t timestamp) {
    uint8_t i = 0;
    for(i < hard_forks.size(); i++;) {
        if (height > hard_forks[i].height) {
          return i;
        }
    }
  }

  Block Blockchain::getLastBlock(std::string ck) const {
      getBlockByHeight(getBlockCount-1,ck);
  }
  Block Blockchain::getBlockDataForHeight(uint64_t height, std::string ck) {
     return getBlockByheight(height,ck);
  }
}