#include <sodium.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>
namespace wallet
{

class wallet
{
public:
	wallet_t createWallet(void);
}

struct wallet_t
{
	uint64_t secret_key;
	uint64_t public_key;
	std::string address;
	uint64_t balance;
};

} // namespace wallet
