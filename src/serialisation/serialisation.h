#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include "src/core/transaction.cpp"
#include "src/core/currency.cpp"
#include "src/core/txmempool.cpp"
#include "src/p2p/peerlist.cpp"

class serialisation {
Public:
  std::optional<string> serialiseAccount(Account acc);
  std::optional<Account> deserialiseAccount(std::string accS);
};
