#include "src/config.h"
#include "serialisation.h"
#include "src/core/transaction.cpp"
#include "src/core/currency.cpp"
#include "src/core/account.cpp"
#include <vector>
#include <string>
std::string seriliase(std::vector<std::string> data) {
	std::string out;
	for (uint64_t i = 0; i < vector.size(); i ++) {
    	out << vector.at(i) + "|";
	}
  return out;
}

std::vector<string> deSeriliase(std::string in) {
	std::stringstream ss(in);
	std::vector<std::string> result;

	while (ss.good())
	{
		std::string substr;
		getline(ss, substr, '|');
		result.push_back(substr);
	}
	return result;
}	
std::optional<string> serialiseAccount(Account acc)
{
	if (!verifyAccount(acc))
		return;

	std::string accS; // Account serialised
	int ss = sprintf(accS, std::to_string(acc.balance) + "|" + std::to_string(acc.state) + "|" + acc.username);
	return accS;
}

std::optional<Account> deserialiseAccount(std::string accS)
{
	std::stringstream ss(buffer);
	std::vector<std::string> result;

	while (ss.good())
	{
		std::string substr;
		getline(ss, substr, '|');
		result.push_back(substr);
	}

	Account acc;
	acc.balance = std::stoi(result.at(0));
	acc.state = std::stoi(result.at(1));
	acc.username = result.at(2);
	return acc;
}
// block header

std::optional<string> serialiseHeader(Blockheader h)
{
	std::string hs; // serialised
	int ss = sprintf(hs, std::to_string(h.version) + "|" + h.chainKey + "|" + h.prevHash + "|" + std::to_string(h.timestamp));
	retrun hs;
}

std::optional<Blockheader> deserialiseHeader(std::string hs)
{
	std::stringstream ss(buffer);
	std::vector<std::string> result;

	while (ss.good())
	{
		std::string substr;
		getline(ss, substr, '|');
		result.push_back(substr);
	}

	Blockheader h;

	h.version = std::stoi(result.at(0));
	h.chainKey = result.at(1);
	h.prevHash = result.at(2);
	h.timestamp = std::stoi(result.at(3));

	return h;
}

// transaction
std::optional<string> serialiseTransaction(Transaction tx)
{
	std::string txs; // serialised
	int ss = sprintf(txs, std::to_string(tx.amount) + "|" + tx.senderKey + "|" + tx.recieverKey + "|" + std::to_string(tx.unlockTime) + "|" + tx.extraData + "|" + std::to_string(tx.gasPrice) + "|" + std::to_string(tx.maxGas) + "|" + std::to_string(tx.nonce) + "|" + tx.signature);
	return txs;
}

std::optional<Transaction> deserialiseTransaction(std::string txs)
{
	std::stringstream ss(buffer);
	std::vector<std::string> result;

	while (ss.good())
	{
		std::string substr;
		getline(ss, substr, '|');
		result.push_back(substr);
	}

	Transaction tx;
	tx.amount = std::stoi(result.at(0));
	tx.senderKey = result.at(1);
	tx.receiverKey = result.at(2);
	tx.unlockTime = std::stoi(result.at(3));
	tx.extraData = result.at(4);
	tx.gasPrice = std::stoi(result.at(5));
	tx.maxGas = std::stoi(result.at(6));
	tx.nonce = std::stoi(result.at(7));
	tx.signature = result.at(8);

	return tx;
}
