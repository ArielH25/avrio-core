#include <src/config.h>
#include <iostream>
#include <ip2string.h>
#include <src/msg/CliCommon.cpp>
#include <src/P2p/p2p.h>
#include <string>
#include "external/spdlog/spdlog.h"
#include <cctype>
#include "sync.cpp" // TODO
#include "src/common/cliinput.cpp"

struct cliOption
{
	std::string command;
	std::string description;
	void (*function)(void);
};

void printHelp()
{
	for (uint64_t i = 0; i < cliOptions.size(); i++)
	{
		spdlog::info(cliOptions.command.at(i) + cliOptions.description.at(i));
		spdlog::debug(cliOptions.command.at(i) + cliOptions.description.at(i) + " (Calls " + cliOptions.function.at(i) ")");
	}
	return;
}

static const std::vector<cliOption> cliOptions =
	{
		{"help", " Displays this message", printhelp},
		{"syncstatus", " Gets the current sync status", printSyncStatus},
		{"status", " Displays current info about this node", printStatus},
		{"networkstatus", " Dispalys current infoabout the network", printNetworkStatus},
		{"printpeers", " Displays Currently connected peers", printConnections},
		{"printpeerlist", "Displays a list of ALL known peers", printPeerList},
};

void printGenesisTx(std::string rewardAddress)
{
	uint64_t reward = config::PREMINE_AMOUNT;

	if (reward == 0)
	{
		reward = getBlockReward();
		Transaction tx = buildCoinbaseTx(rewardAddress, reward);
		crypto::hash(tx);
	}
	else
	{
		Transaction tx = buildCoinbaseTx(rewardAddress, reward);
		crypto::hash(tx);
	}

	spdlog::info("Genesis TX hash: \n");
	std::cout << tx + "\n";

	return;
}

void onCliInput()
{

	void main()
	{
		cli::cliMain(); // TODO (a "header" that is printed at the begining of every program that includes a ascii art and a license
		spdlog::info("Starting node");
		spdlog::info("INFO: Launching P2p server");

		if (!p2p::launchsrv())
		{
			// Launch failed, abort
			spdlog::error("Failed to start P2p server, exiting...");
			exit(-1);
		}
		else
		{
			spdlog::info("P2p server running with peer ID: " << p2p::getId());
		}

		if (config.fullnode == "false" && config.chainsToSync == nullptr && config.doNotSync == nullptr)
		{	// not a fullnode and chains to (or not to) sync is not defined
			// selectChainsToSync:
			spdlog::info("Please select a option: ");
			spdlog::info("1) sync ALL chains ");
			spdlog::info("2) sync only chains i select ");
			spdlog::info("3) sync all chains other than ones i select ");
			int input;
			std::cin >> input;
			vector<std::string> toSync;
			vector<std::string> doNotSync;
			std::vector<std::string> blank;

			if (input == 2)
			{
				spdlog::info("Enter Chain hashes (seperated by a comma (), NO spaces: ");
				std::stringstream chains;

				std::cin >> chains while (chains.good())
				{
					std::string ss;
					getline(chains, ss, ',');
					toSync.push_back(ss);
					free(doNotSync);
				}
			}
			else if (input == 3)
			{
				spdlog::info("Enter Chain hashes not to sync (seperated by a comma (,) NO spaces: ");
				std::stringstream chains;
				std::cin >> chains while (chains.good())

				{
					std::string ss;
					getline(chains, ss, ',');
					doNotSync.push_back(ss);
					free(toSync);
					free(blank);
				}
			}
			else if (input == 1)
			{
				free(doNotSync);
				free(toSync);
			}
			else
			{
				spdlog::info("Bad option");
				goto selectChainsToSync;
			}

			spdlog::info("Please veify the following info is correct:");
			spdlog::info(" You are about to sync:");

			if (input == 2)
			{
				spdlog::info("  Chains:");
				for (uint64_t i = 0; i < toSync.size; i++)
				{
					spdlog::info("  " + toSync.at(size));
				}
			}
			else if (input == 3)
			{
				spdlog::info("  All chains other than:")
				{
					for (uint64_t i = 0; i < doNotSync.size; i++)
					{
						spdlog::info("  " + doNotSync.at(size));
					}
				}
			}
			else
			{
				spdlog::info("  All chains.");
			}

			spdlog::info("Is this Correct? (Y/n)");
			std::string in;

			while (in != 'y' or in != 'Y' or in != 'n' or in != 'N')
			{
				spdlog::info("Please enter your answer: ");
				cin >> in;
			}

			toUpper(in);

			if (in != 'Y')
			{
				spdlog::info("Cancling...");
				goto selectChainstoSync;
			}

			if (input == 1)
				sync(blank, "ALL"); // Mode ALL == sync all

			if (input == 2)
				sync(toSync, "SL"); // Mode SL == sync list as input

			if (input == 3)
				sync(doNotSync, "DNS") // Mode DNS = do not sync list as input
					spdlog::info("Syncing started, to get progress type sync status and hit enter");
		}
		else if (config.fullnode == "true")
		{
			std::vector blank;
			sync(blank, "ALL");
			spdlog::info("Syncing started, to get progress type sync status and hit enter");
		}
		else if (config.doNotSync != nullptr)
		{
			sync(config.doNotSync, "DNS");
			spdlog::info("Syncing started, to get progress type sync status and hit enter");
		}
		else if (config.doNotSync != nullptr)
		{
			sync(config.toSync, "SL");
			spdlog::info("Syncing started, to get progress type sync status and hit enter");
		}

		std::thread cliinputthread(cliinput::main, &onCliInput); // Launch cli input thread
		//TODO: finish daemon code
	}
}
