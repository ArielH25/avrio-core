namespace config
{

std::string currencyName = "avrio";
uint64_t maxTxExtraSize = 2500000;			// the amount of (extra) data that can be attached to a tx (2.5 kb) (eg for smart contracts)
uint64_t decimalPlaces = 4;
uint64_t blockMajorVersion = 0;
uint64_t blockMinorversion = 0;
uint64_t nodeRewardInterval = 60 * 60 * 50;	// 50 Hours per reward
uint64_t maxReward = 2500;					// The max reward a node can win is 2.5
int      minGasPrice = 1;					// 0.0001 aio

} // namespace config
