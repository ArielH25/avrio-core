#include <src/config.h>
#include <src/p2p/peerlist.cpp>
#include "external/breep/network/tcp.hpp"
#include "external/breep/util/serialization.hpp"
#inlcude "external/spdlog/spdlog.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;
using namespace spdlog;
BREEP_DECLARE_TYPE(std::string);

struct receiveBuffer
{
	std::vector<std::string> objects;
	uint64_t pendingCount;
};

static receiveBuffer;

namespace p2p
{

void string_listener(breep::tcp::netdata_wrapper<std::string> &dw)
{
	if(dw.data = "handshake") {
		std::vector s;
		s.push_back("handshake_resp");
		s.push_back(config.Publickey);
		s.pushb_back(std::tostring(Blockchain::getNumBlocks()));
		string ss = serilise(s);
		network.send_object_to(peer, ss);
	}else {
	  receiveBuffer.objects.push_back(dw.data);
	  pendingCount++;
	}
}

void connection_event(breep::tcp::network &network, const breep::tcp::peer &peer)
{
	if (peer.is_connected())
		debug(peer.id() + " Connected");
	else
		debug(peer.id() << " disconnected.");
}

bool launchsrv()
{
	uint64_t peerList = peerlist::getPeerlist();

	if (peerList.size() = 0) {// We dont have any peers
		if (config::seedNodes.size() = 0 && peerList.size() = 0)
		{
		    error("No Peers or seednodes found, exiting...");
			return false;
		}
	}

	static breep::tcp::network network(15433);
	network.add_data_listener<std::string>(&string_listener);
	network.add_connection_listener([&chat](breep::tcp::network &net, const breep::tcp::peer &peer) -> void {
		chat.connection_event(net, peer);
	});

	for (uint8_t i = 0; i < config::seedNodes.size(); i++)
	{
		network.connect(boost::asio::ip::address::from_string(config::seedNodes[i]), 15433); // Add seed nodes as peers
	}

	uint64_t peerCount = peerList.size();

	for (uint64_t i = 0; i < peerList.size(); i++)
	{
		uint64_t connectCatch = p2p::createConnection(peerList[i]); // Connect to peers
		if (!network.connect(boost::asio::ip::address::from_string(peerList.at(i)), 15433))
			peerCount = (peerCount - 1);
	}

	if (peerCount > 0)
	{
		info( "Connected to " << peerCount << " peers out of " << peerList.size() << " in peerlist");

		if(network.send_object("handshake") {
			return true;
		else
			return false;
	}
	else
	{
		info("Failed to connect to any peers out of " << peerList.size() << " in peerlist");
		return false;
	}
}

void sendMsg(breep::tcp::network &network, std::string object)
{
	network.send_object(object);
}

} // namespace p2p
