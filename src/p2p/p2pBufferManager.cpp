// Copywrite The Avrio core team 2019
/* This file handles the processing of objects recieved from
   peers. It:
   * gets data type (if relivant)
   * deseriliases data
   * exectutes the command relavant to thta data type (eg addNewblock(Block newBlock);)
   * if relivant brocasts to all of its connected peers (other than the one it recived it from)
*/

#include <string>
#include <iostream>
#include <vector>
#include <ctime>
#include "p2p.cpp"
#include "serialisation/serialisation.cpp"
#import <thread>

using namespace std;
using namespace p2p;

void bufferManager()
{
	while (config.running)
	{
		if (receiveBuffer.pendingCount != 0)
		{
			std::string objectS = receiveBuffer.back();
			uint64_t i = 0;

			while (str.substr(0, i) != "|")
				i++;

			std::string type = str.substr(0, i - 1);
			std::string object = deserialiaseObject(object, type); // todo
			if (type == "newBlock")
			{
				if (Blockchain::addblock(object, object.chainKey))
					sendObject(objectS);
			}

			if (type == "newPeer")
			{
				peerlist::addPeer(objectS); //todo
				p2p::addPeer(objectS);		//todo
			}

			if (type == "uptimeProof") {
				if(isFullNode(object.nodeKey))
					addUptimeProof(object); //todo todo
			}

			if (type == "newFullNode")
				addFullNode(object); // todo

			if (type == "deregisterFullNode")
				deregisterFullNode(object.chainKey); // todo
		}
	}
}

if (config.running)
	std::thread bm(bufferManager);
}
