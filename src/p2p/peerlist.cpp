#include "peerlist.h"

namespace peerlist
{

uint8_t getPeerlist()
{
	folder = getFolder();
	ifstream file(folder + "peerlist.txt");
	if (file.is_open())
	{
		std::uint32_t peerList[];
		while (file >> peerList[i]; i++;)
	}
	return PeerList;
}

int addPeer(uint8_t peer)
{
	folder = getFolder();
	ifstream file(folder + "peerlist.txt");

	if (file.is_open())
		file << peer;

	if (!greyList.peerList.push_back(peer))
		return 0;

	std::cout << "INFO: Succsessfully Added peer: " << peer;

	if (connectPeer(peer) {
		peerList.erase(std::remove(peerList.greylist.begin(), peerList.greylist.end(), peer), peerList.greylist.end());
		peerList.whitelist.push_back(peer);
    }

    return 1;
}

} // namespace peerlist
